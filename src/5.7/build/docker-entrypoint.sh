#!/bin/bash
set -e

mysqld_cmd="/usr/sbin/mysqld"
mysql_cmd="/usr/bin/mysql --protocol=socket -uroot -hlocalhost --socket=/var/run/mysqld/mysqld.sock"

MYSQL_DATADIR=${MYSQL_DATADIR:-"/var/lib/mysql"}

if [ "$(id -u)" = '0' ]; then

    echo 'Initializing MySQL'

    mkdir -p "${MYSQL_DATADIR}"
    chown -R mysql:mysql "$MYSQL_DATADIR"


    exec gosu mysql "$BASH_SOURCE"
fi

if [ ! -d "$MYSQL_DATADIR/mysql" ]; then

    echo 'Initializing empty database'
    "$mysqld_cmd" --initialize-insecure
    echo 'Database initialized'

    MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD:-""}
    MYSQL_DATABASE=${MYSQL_DATABASE:-""}
    MYSQL_USER=${MYSQL_USER:-""}
    MYSQL_PASSWORD=${MYSQL_PASSWORD:-""}

    "$mysqld_cmd" --skip-networking --socket=/var/run/mysqld/mysqld.sock &
    pid="$!"

    for i in {30..0}; do
        if echo 'SELECT 1' | ${mysql_cmd} &> /dev/null; then
            break
        fi
        echo 'MySQL init process in progress...'
        sleep 1
    done
    if [ "$i" = 0 ]; then
        echo >&2 'MySQL init process failed.'
        exit 1
    fi

    # mysql_tzinfo_to_sql /usr/share/zoneinfo | sed 's/Local time zone must be set--see zic manual page/FCTY/' | "${mysql_cmd} mysql"

${mysql_cmd} <<-EOSQL
    -- What's done in this file shouldn't be replicated
    --  or products like mysql-fabric won't work
    SET @@SESSION.SQL_LOG_BIN=0;
    DELETE FROM mysql.user ;
    CREATE USER 'root'@'%' IDENTIFIED BY '${MYSQL_ROOT_PASSWORD}' ;
    GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION ;
    DROP DATABASE IF EXISTS test ;
    FLUSH PRIVILEGES ;
EOSQL

    mysql_cmd+=" -p"
    mysql_cmd+=${MYSQL_ROOT_PASSWORD}

    ansible-playbook /opt/ansible/basic/playbook.yml

    if ! kill -s TERM "$pid" || ! wait "$pid"; then
        echo >&2 'MySQL init process failed.'
        exit 1
    fi


fi

exec /usr/sbin/mysqld